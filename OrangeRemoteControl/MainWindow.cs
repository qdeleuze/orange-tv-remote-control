﻿///-----------------------------------------------------------------
///   Class:          MainWindow.cs
///   Description:    Orange Decoder remote control (GTK#/HTTP request)
///   Author:         Quentin Deleuze <quentindeleuze@outlook.com>
///   Date: 01/05/2016
///   Revision History:
///   Name: Orange Decoder remote control (GTK#/HTTP request) Date: 01/05/2016  Description: gui, btn handler, http request
///-----------------------------------------------------------------

using System;
using System.Net;
using Gtk;

public partial class MainWindow: Gtk.Window
{
	public MainWindow () : base (Gtk.WindowType.Toplevel)
	{
		Build ();
	}

	public void SendRequest (int keyCode, int modeNumber)
	{
		string _ip = this.ipEntry.Text;
		string url = "http://" + _ip + ":8080/remoteControl/cmd?operation=01&key=" + keyCode + "&mode=" + modeNumber;
		string response;

		using (WebClient client = new WebClient ())
		{
			try
			{
				response = client.DownloadString(url);
				Console.WriteLine(response);
			}
			catch (WebException e)
			{
				MessageDialog msgBox = new MessageDialog (this, DialogFlags.Modal, MessageType.Error, ButtonsType.Ok, "Wrong IP adress or network problem" + Environment.NewLine + e.ToString());
				msgBox.Title = "Error";
				msgBox.Run ();
				msgBox.Destroy ();
			}
		}
	}

	protected void OnDeleteEvent (object sender, DeleteEventArgs a)
	{
		Application.Quit ();
		a.RetVal = true;
	}

	protected void OnMenuBtnClicked (object sender, EventArgs e)
	{
		SendRequest (139, 0);	
	}

	protected void OnReturnBtnClicked (object sender, EventArgs e)
	{
		SendRequest (158, 0);
	}

	protected void OnVolumeUpClicked (object sender, EventArgs e)
	{
		SendRequest (115, 0);
	}

	protected void OnVolumeDownClicked (object sender, EventArgs e)
	{
		SendRequest (114, 0);
	}

	protected void OnVolumeMuteClicked (object sender, EventArgs e)
	{
		SendRequest (113, 0);
	}

	protected void OnChanUpClicked (object sender, EventArgs e)
	{
		SendRequest (402, 0);
	}

	protected void OnChanDownClicked (object sender, EventArgs e)
	{
		SendRequest (403, 0);
	}

	protected void OnVodBtnClicked (object sender, EventArgs e)
	{
		SendRequest (393, 0);
	}

	protected void OnOneBtnClicked (object sender, EventArgs e)
	{
		SendRequest (513, 0);
	}

	protected void OnTwoBtnClicked (object sender, EventArgs e)
	{
		SendRequest (514, 0);
	}

	protected void OnThreeBtnClicked (object sender, EventArgs e)
	{
		SendRequest (515, 0);
	}

	protected void OnFourBtnClicked (object sender, EventArgs e)
	{
		SendRequest (516, 0);
	}

	protected void OnFiveBtnClicked (object sender, EventArgs e)
	{
		SendRequest (517, 0);
	}

	protected void OnSixBtnClicked (object sender, EventArgs e)
	{
		SendRequest (518, 0);
	}

	protected void OnSevenBtnClicked (object sender, EventArgs e)
	{
		SendRequest (519, 0);
	}

	protected void OnEightBtnClicked (object sender, EventArgs e)
	{
		SendRequest (520, 0);
	}

	protected void OnSevenBtn1Clicked (object sender, EventArgs e)
	{
		SendRequest (521, 0);
	}

	protected void OnZeroBtnClicked (object sender, EventArgs e)
	{
		SendRequest (512, 0);
	}

	protected void OnBackBtnClicked (object sender, EventArgs e)
	{
		SendRequest (168, 0);
	}

	protected void OnPlaypauseBtnClicked (object sender, EventArgs e)
	{
		SendRequest (164, 0);
	}

	protected void OnForwardBtnClicked (object sender, EventArgs e)
	{
		SendRequest (159, 0);
	}

	protected void OnStopBtnClicked (object sender, EventArgs e)
	{
		SendRequest (164, 0);
	}

	protected void OnShutdownBtnClicked (object sender, EventArgs e)
	{
		SendRequest (116, 0);
	}

	protected void OnRecBtnClicked (object sender, EventArgs e)
	{
		SendRequest (167, 0);
	}

	protected void OnUpBtnClicked (object sender, EventArgs e)
	{
		SendRequest (103, 0);
	}

	protected void OnDownBtnClicked (object sender, EventArgs e)
	{
		SendRequest (108, 0);
	}

	protected void OnLeftBtnClicked (object sender, EventArgs e)
	{
		SendRequest (105, 0);
	}

	protected void OnRightBtnClicked (object sender, EventArgs e)
	{
		SendRequest (106, 0);
	}

	protected void OnOkBtnClicked (object sender, EventArgs e)
	{
		SendRequest (352, 0);
	}
}
